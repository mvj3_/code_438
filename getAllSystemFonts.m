+ (NSArray*)getAllSystemFonts;
{
    NSMutableArray *array = [[[NSMutableArray alloc] init] autorelease];
    NSArray* familys = [UIFont familyNames];
 
    for (id obj in familys) {
        NSArray* fonts = [UIFont fontNamesForFamilyName:obj];
        for (id font in fonts)     
        {
            [array addObject:font];
        }
    }
    return array; 
}
 
+ (UIFont*)getCurrentFont
{
    //判断系统字体的size，返回使用的字体。
    UIFont *font = [UIFont systemFontOfSize:[UIFont systemFontSize]];
    return font;
}